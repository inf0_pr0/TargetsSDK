// swift-tools-version: 5.8
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "TargetsSDK",
    platforms: [
        .iOS(.v14)
    ],
    products: [
        .library(
            name: "TargetsSDK",
            targets: ["TargetsSDK"]),
    ],
    targets: [
        .binaryTarget(
            name: "TargetsSDK",
            url: "https://st.targets.plus/media/builds/TargetsSDK.xcframework-1.0.4-71.zip",
            checksum: "02d3985bad8f5606ab52bf1d708453a14de2f9c111c144cd75e4991d47e89e99"
        )
    ]
)
